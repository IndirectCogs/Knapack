
 main: main.o
	gcc main.o -o main

 main.o: main.c
	gcc -g -c main.c

 clean:
	rm -f main *.o
