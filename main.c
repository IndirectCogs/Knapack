
 // main.c (Knapsack)
 // December Redinger
 // CSE 222
 // Nick Macias
 // 3/4/2021

 // knapsack takes two arguments, the capacity of a bag, and the item file containing the name/value/weight triples for each item.
 
 // Inputs:
 // Capacity of a bag.
 // Item List File

 // Returns:
 // The program prints the maximum possible value the bag can contain
 // The amount of items should be listed as well.


 // includes

 #include <stdio.h>
 #include <string.h>

 // struct definitions

 struct node{

 int weight;
 int val;
 char name[32];

 };

 struct answer{

 int maxvalue;
 int itemcount[128];

 };

 // function prototype

 struct answer maxVal(int capacity, struct node * list);


 // function definition

 void main(int argc, char *argv[])

 {

 // variables

 int bag_capacity=0,objectw,objectv,i=0;
 char *file[100],object[32],line[128];
 FILE *fp;
 struct node items[128];
 struct answer bagmaxval;

 // if there are not 3 arguments, end the program

 if (argc != 3)
	{
	return;
	}
 
 // initialize struct answer

 bagmaxval.maxvalue = 0;
 
 // set bag_capacity equal to argv 1

 sscanf(argv[1],"%d",&bag_capacity);

 // Read from the input file the list of items

 fp = fopen(argv[2],"r"); 

 // split the lines into three pieces of information, object name, value, and weight

 while (fgets(line,128,fp))
	{
	sscanf(line,"%s %d %d", object, &objectv, &objectw);

	// assign these variables to the different members of struct node

	strcpy(items[i].name,object);
	items[i].val = objectv;
	items[i].weight = objectw;
	i++;
	}
 
 // run the maxVal recursive function with bag_capacity and items as the inputs

 bagmaxval = maxVal(bag_capacity,items);

 // if an objects weight is 0, the object doesn't exist
 
 // Print the list of objects available

 printf("ITEMS LIST\n");
 i=0;
 while (items[i].weight != 0)
 	{
		
	strcpy(object,items[i].name);
	objectw = items[i].weight;
	objectv = items[i].val;	
	printf("%s  %d  %d\n====================\n", object, objectv, objectw);
	i++;

	}		
 
 // print the result

 printf("The maximum value for bag capacity %d is %d\n", bag_capacity, bagmaxval.maxvalue);
 i = 0;

 // if bagmaxval.itemcount[i] is not 0, show the item name and the item count.

 while (items[i].weight != 0)
	{
	
	if (bagmaxval.itemcount[i] != 0)
		{

		// copy the name of the item into object
		strcpy(object,items[i].name);
		printf("Item Name: %s  Item Count: %d\n",object,bagmaxval.itemcount[i]);

		}
	i++;
	}

 // return

 return;

 }
 


 // maxVal returns a struct answer, and accepts an integer and a struct node

 struct answer maxVal(int capacity, struct node * list)

 {

 // variables go here

 int i=0, j=0, newcap;
 struct answer best, try;

 // initialize struct answers
 best.maxvalue = 0;
 try.maxvalue = 0;

 // this is the base recursion case
 if (capacity == 0)
	{
	// return best - I think.
	return best;
	}

 // iterate through every item in the main list - the struct node passed to this function
 // only consider items with a weight greater than 0

 while (list[i].weight != 0)
	{
 	// try is the value of the item, plus the maxVal of the capacity minus item weight
	
 	// only consider items with a weight lower than capacity
	
	if (list[i].weight <= capacity)
		{

 		// newcap is equal to capacity - list[i].weight
		newcap = capacity - list[i].weight;
 
 		// set value equal to maxVal of newcap
		try = maxVal(newcap,list);
 		// add list[i].val to value.maxvalue
		try.maxvalue = try.maxvalue + list[i].val;		

 		// if value.maxvalue is greater than maximumVal.maxvalue
		if (try.maxvalue > best.maxvalue)
			{

 			// set maximumVal.maxvalue equal to value.maxvalue
			best.maxvalue = try.maxvalue;
 			// copy the itemcount over element by element
			while (list[j].weight != 0)
				{
				best.itemcount[j] = try.itemcount[j];
				j++;
				}
 			// iterate the itemcount of maximumVal.itemcount[i]
			best.itemcount[i]++;
			}
		
		}
	// iterate i
	i++;
	}
 // return maximumVal
 return best; 
 }

